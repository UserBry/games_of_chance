
function helper(x,y,z)
{
    let newElement;
    newElement = document.createElement(x);

    newElement.textContent = y;
    document.getElementById(z).appendChild(newElement);
}
function helper1(w,x,y,z)
{
    let newElement;
    newElement = document.createElement(w);
    newElement.style.color = x;
    newElement.textContent = y;
    document.getElementById(z).appendChild(newElement);
}
function helper2(w,x,y,z)
{
    let newElement;
    newElement = document.createElement(w);
    newElement.id = x;
    newElement.textContent = y;
    document.getElementById(z).appendChild(newElement);
}
//=====================================================================



const rpsStart = document.getElementById("rpsStart");
rpsStart.onclick = function()
{

    helper("p","Pick an option please","d2");

    const rock_pic = document.getElementById("rock_pic");
    rock_pic.onclick = function()
    {  
        //Human on left, Bot on right       Rock = 1, Paper =2, Scissors = 3
        let bot = Math.floor((Math.random() * 3) + 1);
        
        switch(bot)
        {
            case 1:
                helper1("p","blue","You both picked ROCK! Play again","d2");
                break;
            case 2:
                helper1("p","red","Bot picked PAPER. Bot wins!","d1d");
                break;
            case 3:
                helper1("p","green","Bot picked SCISSORS. You win!","d1c");
                break;
        }
    }

    const paper_pic = document.getElementById("paper_pic");
    paper_pic.onclick = function()
    {  
        //Human on left, Bot on right      Rock = 1,  Paper = 2, Scissors = 3
        let bot = Math.floor((Math.random() * 3) + 1);
        
        switch(bot)
        {
            case 1:
                helper1("p","green","Bot picked ROCK. You win!","d1c");
                break;
            case 2:
                helper1("p","blue","You both picked PAPER! Play again","d2");
                break;
            case 3:
                helper1("p","red","Bot picked SCISSORS. Bot wins!","d1d");
                break;
        }
    }

    const scissors_pic = document.getElementById("scissors_pic");
    scissors_pic.onclick = function()
    {  
        //Human on left, Bot on right      Rock = 1,  Paper = 2, Scissors = 3
        let bot = Math.floor((Math.random() * 3) + 1);
        
        switch(bot)
        {
            case 1:
                helper1("p","red","Bot picked ROCK. Bot wins!","d1d");
                break;
            case 2:
                helper1("p","green","Bot picked PAPER. You win!","d1c");
                break;
            case 3:
                helper1("p","blue","You both picked SCISSORS! Play again.","d2");
                break;
        }
    }
}
